# Restaurant_Chatbot

Before running you must install the following packages:
- NLTK
- spacy
- keras
- slackclient

You must also run the following command:
- `python3 spacy -m download en`

You can then run the chatbot by running `main.py`.  Then
simply add the bot to your Slack room and you can 
use it to find resaurants that you are interested in.

Remember that you need to @restaurant_bot first and then 
type in your command.