# Response.py
# Intro to Artificial Intelligence
# 15 December 2019

import requests
import json
import random

class Response:

    def __init__(self):
        # Initialize request parameters
        self.apiKey = '3907fe54ddfbd77701c8b3846b4443b7'
        self.cityId = '724' # This is the city ID for South Bend, IN
        self.baseURL = 'https://developers.zomato.com/api/v2.1/'
        self.header = {'User-agent': 'curl/7.43.0', 'Accept': 'application/json', 'user_key': self.apiKey}
        
        # Get all numerical cuisine IDs from API
        r = requests.get(url=self.baseURL + 'cuisines?city_id=' + self.cityId, headers=self.header).json()['cuisines']
        self.cuisines = {}
        for item in r:
            self.cuisines[item['cuisine']['cuisine_name'].lower()] = int(item['cuisine']['cuisine_id'])

        # Create local list of restaurants for faster lookup
        self.restaurants = []
        i = 0
        r = requests.get(url=self.baseURL + 'search?entity_id=' + self.cityId + '&entity_type=city&start=' + str(i) + '&count=20', headers=self.header).json()
        numPlaces = r['results_found']
        r = r['restaurants']
        while i <= numPlaces:
            for place in r:
                place = place['restaurant']
                self.restaurants.append({'name': place['name'], 'id': place['id'], 'cuisines': place['cuisines'].split(',')})
            i = i + 20
            r = requests.get(url=self.baseURL + 'search?entity_id=' + self.cityId + '&entity_type=city&start=' + str(i) + '&count=20', headers=self.header).json()['restaurants']
            print('Building: ' + str(i) + '/' + str(numPlaces))
        for place in self.restaurants:
            for k, cuisine in enumerate(place['cuisines']):
                try:
                    place['cuisines'][k] = self.cuisines[cuisine.strip().lower()]
                except:
                    place['cuisines'][k] = 0

        # Initialize some response lists
        self.greeting_responses = [
            'Hello',
            'Hi',
        ]
        self.closing_responses = [
            'Goodbye',
            'Bye',
            'Thank you for using me for your restaurant needs!'
        ]
        
    # Generate a response to a greeting
    def respond_greeting(self):
        return self.greeting_responses[random.randint(0, len(self.greeting_responses) - 1)] + '\nType help to see what I can do!'

    # Generate a response to a closing
    def respond_closing(self):
        return self.closing_responses[random.randint(0, len(self.closing_responses) - 1)]

    # Generate a response to a restuarant search by cuisine type by the user
    def respond_restaurant_search(self, entities):
        try:
            message = ''
            count = 0
            cuisine = self.cuisines[entities['food_types'].lower()]
            for ir in self.restaurants:
                for ic in ir['cuisines']:
                    if ic == cuisine:
                        message = message + ir['name'] + '\n'
                        count = count + 1
            message = message + '\nIf you would like to find out more about one of these restaurants, just ask about it!\n'
            message = 'There are ' + str(count) + ' restaraunts in South Bend that serve ' + entities['food_types'] + ' food.\n\n' + message
            return message
        except:
            return 'Oops, something went wrong. Try again.'

    # Generate a response to a review request for a certain restaurant
    def respond_restaurant_reviews(self, entities):
        try:
            restId = 0
            for ir in self.restaurants:
                if ir['name'].lower() == entities['restaurant'].lower():
                    restId = ir['id']
                    break
            response = requests.get(url = self.baseURL + 'reviews?res_id=' + str(restId), headers = self.header).json()
            if len(response['user_reviews']) >= 1:
                message = 'We were able to find the following reviews for ' + entities['restaurant'] + ':\n\n'
                for review in response['user_reviews']:
                    review = review['review']
                    message = message + str(review['rating']) + ' Stars: ' + review['rating_text'] + '\n'
                    message = message + review['review_text'] + '\n\n'
                return message
            return 'Unfortunately, no reviews could be found for this restaurant.  Maybe you can be the first to write one!'
        except:
            return 'Oops, something went wrong. Try again.'


    # Generate a response to a menu request for a specific restaurant
    def respond_restaurant_menu(self, entities):
        try:
            restId = '0'
            for ir in self.restaurants:
                if ir['name'].lower() == entities['restaurant'].lower():
                    restId = ir['id']
                    break
        except:
            return 'Oops, something went wrong. Try again.'
        response = requests.get(url = self.baseURL + 'dailymenu?res_id=' + str(restId), headers = self.header).json()
        try:
            message = 'Todays menu at ' + entities['restaurant'] + 'contains the following items:\n\n'
            for item in response['daily_menu'][0]['dishes']:
                message = message + item['name'] + ' - ' + item['price'] + '\n'
            message = message + '\n' + 'We hope that you found something that sounds good!\n'
            return message
        except:
            message = 'No menu was provided, but the following things will be available at ' + entities['restaurant'] + ':\n\n'
            response = requests.get(url = self.baseURL + 'restaurant?res_id=' + str(restId), headers = self.header).json()
            for item in response['highlights']:
                message = message + item + '\n'
            message = message + '\nWe apologize that no menu could be found.  Hopefully something from the list above is helpful!\n'
            return message
        return 'Unfortunately, ' + entities['restaurant'] + ' has not provided us with their menu.\n'

    # Generate a response to a restaurant info request
    def respond_restaurant_info(self, entities):
        restId = '0'
        try:
            for ir in self.restaurants:
                if ir['name'].lower() == entities['restaurant'].lower():
                    restId = ir['id']
                    break
            response = requests.get(url = self.baseURL + 'restaurant?res_id=' + str(restId), headers = self.header).json()
        except:
            return 'Oops, something went wrong. Try again.'
        try:
            return ('Here is some information about that restaurant:\n\n'
                    '' + entities['restaurant'] + '\n'
                    '' + response['location']['address'] + '\n'
                    '' + response['phone_numbers'] + '\n\n'
                    'For two people, this restaurant usually costs around $' + str(response['average_cost_for_two']) + '.\n'
                    'It has been rated ' + response['user_rating']['aggregate_rating'] + '/5 stars by others in your area.\n\n'
                    'To view reviews or a menu, just ask!\n')
        except:
            return 'Oops, something went wrong. Try again.'
    # Respond to a help request
    def respond_help(self):
        return ('I can respond to the following types of requests:\n'
                '- Please show me restaurants with Chinese food.\n'
                '- Can you show me the menu for O\'Rourkes Public House?\n'
                '- What kinds of reviews have people written for the Fiddler\'s Harth restaurant?\n'
                '- Give me some more information about Brothers Bar and Grill\n'
                '\n'
                'To end your session, you can simply type \"Goodbye\"')
    
    # Generate a response for any unrecognized message from the user
    def respond_other(self):
        return 'I\'m sorry, I do not understand.  Please rephrase your request.'

    # Takes string 'classification' from the classifier and a list of strings 'entities' from the entity recognizer
    # Passes entities down the correct branch of the tree based on the classification string
    def get_response(self, classification, entities):
        if classification == 'greeting':
            return self.respond_greeting()
        elif classification == 'closing':
            return self.respond_closing()
        elif classification == 'restaurant_search':
            return self.respond_restaurant_search(entities)
        elif classification == 'restaurant_reviews':
            return self.respond_restaurant_reviews(entities)
        elif classification == 'restaurant_menu':
            return self.respond_restaurant_menu(entities)
        elif classification == 'restaurant_info':
            return self.respond_restaurant_info(entities)
        elif classification == 'help':
            return self.respond_help()
        else:
            return self.respond_other()