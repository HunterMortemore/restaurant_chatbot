#Keep this code running and talk to the chatbot in any channel or its chatbox. eg. "@restaurant_bot hey"

import time
from slackclient import SlackClient

class Command(object):
    def __init__(self):
        self.commands = { 
            "hello" : self.hello,
            "bye" : self.bye,
            "help" : self.help
        }
 
    def handle_command(self, user, command):
        response = "<@" + user + ">: "
     
        if command in self.commands:
            response += self.commands[command]()
        else:
            response += "Sorry I don't understand the command: " + command + ". " + self.help()
         
        return response
         
    def hello(self):
        return "Heyy !!"

    def bye(self):
        return "Sad to see you go, bye"
     
    def help(self):
        response = "Currently I support the following commands:\r\n"
         
        for command in self.commands:
            response += command + "\r\n"
             
        return response

class Event:
    def __init__(self, bot):
        self.bot = bot
        self.command = Command()
     
    def wait_for_event(self):
        events = self.bot.slack_client.rtm_read()
        
        if events and len(events) > 0:
            for event in events:
                #print event
                print('I am listening')
                self.parse_event(event)
                 
    def parse_event(self, event):
        print(event)
        print(self.bot.bot_id)
        if event and 'text' in event and self.bot.bot_id in event['text']:
            self.handle_event(event['user'], event['text'].split(self.bot.bot_id)[1].strip().lower(), event['channel'])
     
    def handle_event(self, user, command, channel):
        if command or channel:
            print("Received command: " + command + " in channel: " + channel + " from user: " + user)
            response = self.command.handle_command(user, command)
            self.bot.slack_client.api_call("chat.postMessage", channel=channel, text=response, as_user=True)

 
class Bot(object):
    def __init__(self):
        slack_token = "xoxb-17641790740-861275227906-3t9BeitwclNxqRRDuz5EKmLT"
        self.slack_client = SlackClient(token=slack_token)
        self.bot_name = "restaurant_bot"
        self.bot_id = self.get_bot_id()
         
        if self.bot_id is None:
            exit("Error, could not find " + self.bot_name)
     
        self.event = Event(self)
        self.listen()
     
    def get_bot_id(self):
        api_call = self.slack_client.api_call("users.list")
        if api_call.get('ok'):
            # retrieve all users so we can find our bot
            users = api_call.get('members')
            for user in users:
                if 'name' in user and user.get('name') == self.bot_name:
                    return "<@" + user.get('id') + ">"
             
            return None
             
    def listen(self):
        if self.slack_client.rtm_connect(with_team_state=False):
            print("Successfully connected, listening for commands")
            while True:
                self.event.wait_for_event()
                
                time.sleep(1)
        else:
            exit("Error, Connection Failed") 
Bot()