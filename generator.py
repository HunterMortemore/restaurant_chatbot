import random
from locale import str
f = open('cuisines.txt','r')
categories = f.readlines()
f.close()
categories = [x[:-1] for x in categories]

f = open('restaurants.txt','r')
restaurants = f.readlines()
f.close()
restaurants = [x[:-1] for x in restaurants]

b4start = ['Hi there. ','Hi, ','','','','Hey, ','Hello. ','Hi! ','','Hi ','Morning, ','Today ','Now ']
start = ["I'd like to eat ", 'I would like to have some ', 'I would like to eat ', \
'I would love to have ', 'I would love to eat ', "I'd love to have some ", "I'd love to eat ",\
'I want ', 'I just want ', 'I am willing to eat ', 'I am eager to eat ', 'Can you provide me some ',\
'Provide me some ', 'Give me some ', 'I feel like ', 'I am in the mood of ', 'I go for ',\
'I am craving ', 'I am salivating ', 'I am dying for ',     'I am interested in ',\
'It would be great if I can have some ', 'I fancy ', "I can't get enough of ",\
'I really want ', 'I would love to eat some ','I really want some ','Give me some options to eat ', 'Show me good ',\
'Find me some ','Find me a place to eat ','Show me ','Show me where I can find ','Do you have any place with ']
ending = ['.','!','. Thank you very much.','. Thanks.','. I cannot wait.','.','.','.']
ctype = ['cuisine','food','meal','dinner','supper','lunch','breakfast']
time = ['today','tomorrow']
stime = ['this afternoon','next morning','this evening']
pick = ['this','next']
dow = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday']
ams = ['am','AM',"Am",'a.m.','A.M.']
pms = ['pm','PM','Pm','p.m.',"P.M."]
moneyamount = [10,15,20,25,30,35,40,50,80,100,1000]
mtype = ['under','greater than','more than','less than','around','about','within']
dtype = ['bucks','dollars','$','dollar']

rtype1start = ['Where is the location of ','What is the address of ','I want to go to ','I will go to ','I am planning to go to ',\
    'Where can I find ','How can I make reservations at ', 'How much do people spend at ','How much is the food at ',\
    'Is it expensive to eat at ', 'How do I drive to ','How can I get to ','Are there any bad reviews about ',\
    'What is the atmosphere like at ','How is the food at ','What do people think about ','What are people saying about ',\
    'Can I see a menu from ','How many people have rated ','What kind of food is served in '\
    'Can you show me the food options in ', 'What can I order at ','Do they have vegetarian options at '\
    'Can I eat vegan at ','I am on a diet, what can I eat at ','What can I buy at '\
    'Where is ','Which food items are available at ','What is the phone number of ','How do I call ',\
    'How can I make reservations at ','How much money should I bring to ']

rubbish = ['hi','good morning','hi bot','bye','see you later','goodbye','quit','exit',"how's the weather like today?",\
    'Are you a bot or a real human?','I am hungry','Help me','Yes','No','cool','great','never mind']

rtype2 = [('Is ','good?'),('I am wondering if ','have good food.'),('What kind of food does ','have?'),\
    ("Where is ",'located?'),("What's ",'address'),("What is ",'address'),('Does ','take reservations'),\
    ]
def generateR(i):
    choice = random.randint(0,1)

    #style 1 restaurant
    "Hi, How do I drive to Legends of Notre Dame. "
    if choice == 0:
        t = random.choice(b4start)+random.choice(rtype1start)
        l1 = len(t)
        t += restaurants[i]
        l2 = len(t)
        t += random.choice(['?','.',''])
        return t+'|'+str(l1)+'|'+str(l2)+'|'+'restaurant'
    #style 2
    if choice == 1:
        t = random.choice(b4start)
        j = random.randint(0,len(rtype2)-1)
        t += rtype2[j][0]
        l1 = len(t)
        t += restaurants[i]
        l2 = len(t)
        t += ' '
        t += rtype2[j][1]
        return t+'|'+str(l1)+'|'+str(l2)+'|'+'restaurant'


def generateC(i):
    #choice = random.randint(0,1)

    #style 1 cuisine
    if True:
        t = random.choice(b4start)+random.choice(start)
        l1 = len(t)
        t += categories[i]
        l2 = len(t)
        t += ' '
        t += random.choice(ctype)
        t += getTime()
        return t+'|'+str(l1)+'|'+str(l2)+'|'+'food_types'

def generateRubbish():
    return random.choice(rubbish)

def getCuisine():
    return random.choice(categories)+' '+random.choice(ctype)

def getTime():
    return random.choice([' '+random.choice(time+stime),' '+random.choice(pick)+' '+random.choice(dow),\
        ' '+getSptime(),' for '+random.choice(['breakfast','brunch','lunch','supper','dinner'])])

def getSptime():
    return 'at '+random.choice([str(random.randint(9,11))+random.choice(['',' '])+random.choice(ams),\
         str(random.randint(12,12))+random.choice(['',' '])+random.choice(pms)])+\
            random.choice(['',' '+random.choice(time),' '+random.choice(pick)+' '+random.choice(dow)])

def getMoney():
    return random.choice([' '+random.choice(mtype)+' '+\
        random.choice(['$'+random.choice(['',' '])+str(random.choice(moneyamount)),\
            str(random.choice(moneyamount))+random.choice(['',' '])+random.choice(dtype)])])

def getRestaurant():
    return 'at '+random.choice(restaurants)



f = open('entity_recognition_train.txt','w')
for i in range(100):
    f.write(generateR(i))
    f.write('\n')
    if random.randint(0,5)==0:
        f.write(generateRubbish())
        f.write('\n')
for i in range(57):
    f.write(generateC(i))
    f.write('\n')
f.close()