# trainClassifier.py
# Intro to AI
# 16 December 2019

# Imports
#import tensorflow as tensorflow
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.models import Sequential
from tensorflow.keras import layers
from sklearn.preprocessing import LabelBinarizer
import numpy as np

class Classifier:

    def __init__(self):
        # Read in training data
        inTrain = []
        outTrain = []
        data = open('classificationData.txt', 'r')
        for line in data:
            line = line.strip().split(':')
            inTrain.append(line[1].lower())
            outTrain.append(line[0].lower())

        # Perform tokenization on training data inputs
        self.tokenizer = Tokenizer(num_words=1000)
        self.tokenizer.fit_on_texts(inTrain)
        x_train = self.tokenizer.texts_to_matrix(inTrain)

        # Perform one hot encoding on training data outputs
        self.encoder = LabelBinarizer()
        self.encoder.fit(outTrain)
        y_train = self.encoder.transform(outTrain)

        # Construct Model
        self.model = Sequential()
        self.model.add(layers.Dense(512, input_shape=(1000,)))
        self.model.add(layers.Activation('relu'))
        self.model.add(layers.Dense(8))
        self.model.add(layers.Activation('softmax'))
        self.model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
        self.model.summary()

        # Train Model
        self.model.fit(x_train, y_train, epochs=100, verbose=True, batch_size=10)

    def classify(self, message):
        message = message.strip().lower()
        text_labels = self.encoder.classes_
        mIn = self.tokenizer.texts_to_matrix([message])
        prediction = self.model.predict(mIn)
        return text_labels[np.argmax(prediction)]