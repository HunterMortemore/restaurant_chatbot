from __future__ import unicode_literals, print_function


#Keep this code running and talk to the chatbot in any channel or its chatbox. eg. "@restaurant_bot hey"

##########NLP part

import plac
import random
from pathlib import Path
import spacy
from spacy.util import minibatch, compounding
import os  
from spacy import displacy
from collections import Counter
import en_core_web_sm
nlp = spacy.load('en_core_web_sm')
TRAIN_DATA = []
f = open('entity_recognition_train.txt','r')
lines = f.readlines()
for x in lines:
    x = x[:-1]
    x = x.lower()
    x = x.split('|')
    if len(x) == 1:
        TRAIN_DATA.append((x[0],{"entities":[]}))
    else:
        TRAIN_DATA.append((x[0],{"entities":[(int(x[1]),int(x[2]),x[3])]}))

"""Set up the pipeline and entity recognizer, and train the new entity."""
random.seed(0)
nlp = spacy.blank("en")  # create blank Language class
print("Created blank 'en' model")
# Add entity recognizer to model if it's not in the pipeline
# nlp.create_pipe works for built-ins that are registered with spaCy
if "ner" not in nlp.pipe_names:
    ner = nlp.create_pipe("ner")
    nlp.add_pipe(ner)
# otherwise, get it, so we can add labels to it
else:
    ner = nlp.get_pipe("ner")

ner.add_label("restaurant")  # add new entity label to entity recognizer
ner.add_label("food_types")

optimizer = nlp.begin_training()
move_names = list(ner.move_names)
# get names of other pipes to disable them during training
other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "ner"]
print(other_pipes)
with nlp.disable_pipes(*other_pipes):  # only train NER
    sizes = compounding(1.0, 4.0, 1.001)
    # batch up the examples using spaCy's minibatch
    for itn in range(30):
        random.shuffle(TRAIN_DATA)
        batches = minibatch(TRAIN_DATA, size=sizes)
        losses = {}
        for batch in batches:
            texts, annotations = zip(*batch)
            nlp.update(texts, annotations, sgd=optimizer, drop=0.35, losses=losses)
        print("Losses", losses)


##########Classifier and response part
from Response import *
from Classifer import *

classifier = Classifier()
r = Response()

##########Slack Part
import os
import time

from slackclient import SlackClient

class Command(object):
    def __init__(self):
        self.commands = { 
            "greeting" : self.hello,
            "closing" : self.bye,
            "help" : self.help,
            "restaurant_search" : self.restaurant_search,
            "restaurant_reviews" : self.restaurant_reviews,
            "restaurant_menu" : self.restaurant_menu,
            "restaurant_info" : self.restaurant_info,
            "other" : self.other
        }
 
    def handle_command(self, user, command):
        response = "<@" + user + ">: "
        Ccategory = classifier.classify(command)
        entities = {}
        doc = nlp(command)
        for ent in doc.ents:
            print(ent.label_, ent.text)
            entities[ent.label_] = ent.text
        response += self.commands[Ccategory](entities)

        return response
    
    #def commands_differentiation(self,category,entity):

        #return self.commands[category](entity)

    def hello(self,entity):
        response = r.respond_greeting()
        return response

    def bye(self,entity):
        response = r.respond_closing()
        return response
     
    def help(self,entity):
        response = r.respond_help()
        return response

    def other(self,entity):
        response = r.respond_other()
        return response

    def restaurant_search(self,entity):
        response = r.respond_restaurant_search(entity)
        return response

    def restaurant_reviews(self,entity):
        response = r.respond_restaurant_reviews(entity)
        return response

    def restaurant_menu(self,entity):
        response = r.respond_restaurant_menu(entity)
        return response

    def restaurant_info(self,entity):
        response = r.respond_restaurant_info(entity)
        return response

class Event:
    def __init__(self, bot):
        self.bot = bot
        self.command = Command()
     
    def wait_for_event(self):
        events = self.bot.slack_client.rtm_read()
        
        if events and len(events) > 0:
            for event in events:

                self.parse_event(event)
                 
    def parse_event(self, event):
        print(event)
        print(self.bot.bot_id)
        if event and 'text' in event and self.bot.bot_id in event['text']:
            self.handle_event(event['user'], event['text'].split(self.bot.bot_id)[1].strip().lower(), event['channel'])
     
    def handle_event(self, user, command, channel):
        if command or channel:
            print("Received command: " + command + " in channel: " + channel + " from user: " + user)
            response = self.command.handle_command(user, command)
            self.bot.slack_client.api_call("chat.postMessage", channel=channel, text=response, as_user=True)

 
class Bot(object):
    def __init__(self):
        slack_token = "xoxb-17641790740-861275227906-3t9BeitwclNxqRRDuz5EKmLT"
        self.slack_client = SlackClient(token=slack_token)
        self.bot_name = "restaurant_bot"
        self.bot_id = self.get_bot_id()
         
        if self.bot_id is None:
            exit("Error, could not find " + self.bot_name)
     
        self.event = Event(self)
        self.listen()
     
    def get_bot_id(self):
        api_call = self.slack_client.api_call("users.list")
        if api_call.get('ok'):
            # retrieve all users so we can find our bot
            users = api_call.get('members')
            for user in users:
                if 'name' in user and user.get('name') == self.bot_name:
                    return "<@" + user.get('id') + ">"
             
            return None
             
    def listen(self):
        if self.slack_client.rtm_connect(with_team_state=False):
            print("Successfully connected, listening for commands")
            while True:
                self.event.wait_for_event()
                 
                time.sleep(1)
        else:
            exit("Error, Connection Failed") 
Bot()